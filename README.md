# Prison Escape - Android Game

## About

Prison Escape is an Action RPG game where the player must beat several prisoners to collect keys to escape from prison.

Created with Unity

## Genre

Action RPG

## Platform

Android

## Gameplay

The Player control a prisoner that needs to escape from prison before the time is out. Player must beat several prisoners to collect 10 keys to getting out of prison. But becareful with the warden patrolling the whole area. when the warden catches you, the game is over. you can win the game if the player can escape from prison.


## Development Team
- Ilham Pratama

## Screenshot

![](images/13.PNG)

![](images/14.PNG)
