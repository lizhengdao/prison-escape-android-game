﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy1Script : MonoBehaviour
{
    //ATTACK
    public LayerMask whatIsEnemies;
    public Transform attackPos;
    public float attackRange;
    Collider2D[] enemiesToDamage;    
    float attacking;

    //STATS
    public float speed;
    public float attack;
    public float health;
    public float startAttack;

    //ANIMATION
    public Animator anim;

    //MOVING
    public float stoppingDistance;
    public float retreatDistance;
    Vector3 tempAnim;

    //PATROL
    private float waitTime;
    public float startWaitTime;
    public float followingPlayer;
    public Transform[] moveSpots;
    private int randomSpot;

    //EFFECT
    public GameObject bloodEffect;
    private float dazedTime;
    public float startDazedTime;
     
    // Start is called before the first frame update
    void Start()
    {
        health = 100;
        speed = 3f;
        waitTime = startWaitTime;
        randomSpot = Random.Range(0, moveSpots.Length);
        GetComponent<thingsScript>();
        GetComponent<DragToMove>();
        tempAnim = transform.position;

        startAttack = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            EnemyDestroyed();
        }

        if (dazedTime <= 0)
        {
            speed = 2.5f;            
        }

        else if(dazedTime >= 0) {
            speed = 0;
            dazedTime -= Time.deltaTime;
        }

        //MOVE 
        if(Vector2.Distance(transform.position, GameObject.FindGameObjectWithTag("Player").transform.position) <= 5)
        {
            transform.position = Vector2.MoveTowards(transform.position, GameObject.FindGameObjectWithTag("Player").transform.position, speed * Time.deltaTime);

            if(attacking <= 0)
            {
                //ATTACK
                enemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange, whatIsEnemies);

                for (int i = 0; i < enemiesToDamage.Length; i++)
                {
                    enemiesToDamage[i].GetComponent<DragToMove>().TakeDamage(attack);
                }
                
                attacking = startAttack;
                //~~~ATTACK
            }

            else
            {
                attacking -= Time.deltaTime;
            }
            
        }

        else 
        {
            transform.position = Vector2.MoveTowards(transform.position, moveSpots[randomSpot].position, speed * Time.deltaTime);
            if(Vector2.Distance(transform.position, moveSpots[randomSpot].position) < 0.2f)
            {
                if (waitTime <= 0)
                {
                    speed = 3f;
                    randomSpot = Random.Range(0, moveSpots.Length);
                    waitTime = startWaitTime;
                }
                else
                {
                    waitTime -= Time.deltaTime;
                    speed = 0f;
                    changeAnim(tempAnim - transform.position);
                    tempAnim = transform.position;
                }
            }
            
        }

        changeAnim(tempAnim - transform.position);
        tempAnim = transform.position;
        //~~~MOVE
        
    }

    private void setAnim(Vector2 setVector)
    {
        anim.SetFloat("horizontal", setVector.x);
        anim.SetFloat("speed", speed);
    }

    public void TakeDamage(float damages)
    {
        Instantiate(bloodEffect, new Vector3(this.transform.position.x, this.transform.position.y, -9), Quaternion.identity);
        health -= damages;
        dazedTime = startDazedTime;
    }

    private void changeAnim(Vector2 direction)
    {
        if(Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
        {
            //RIGHT
            if(direction.x > 0)
            {
                setAnim(Vector2.left);
            }
            //LEFT
            else if (direction.x < 0)
            {
                setAnim(Vector2.right);
            }
        }

        else if(Mathf.Abs(direction.x) < Mathf.Abs(direction.y))
        {
            //UP
            if(direction.y > 0)
            {
                setAnim(Vector2.right);
            }
            //DOWN
            else if(direction.y < 0)
            {
                setAnim(Vector2.right);
            }
        }
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPos.position, attackRange);
    }

    public void EnemyDestroyed()
    {
        Destroy(this.gameObject);
        thingsScript.playerScore += 234;
        thingsScript.benda += 1;
    }
}
