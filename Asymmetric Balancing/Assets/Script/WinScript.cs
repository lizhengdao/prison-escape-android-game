﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinScript : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject star1;
    public GameObject star2;
    public GameObject star3;

    public Text scores;
    public Text key;
    public Text times;

    public static bool win1Star = false;
    public static bool win2Star = false;
    public static bool win3Star = false;

    void Start()
    {
        GetComponent<thingsScript>();

        if(win1Star)
        {
            Destroy(star2);
            Destroy(star3);
        }

        if (win2Star)
        {
            Destroy(star2);            
        }

        if (win3Star)
        {
            Debug.Log("you get all starts");
        }
    }

    // Update is called once per frame
    void Update()
    {
        scores.GetComponent<Text>().text = thingsScript.playerScore.ToString();
        key.GetComponent<Text>().text = thingsScript.benda.ToString();
        times.GetComponent<Text>().text = thingsScript.times.ToString();
    }
}
