﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragToMove : MonoBehaviour
{
    //MOVEMENT    
    public Rigidbody2D rb;
    Vector2 movement;
    public Animator animator;
    int moveTemp;
    bool flipped;
    public Joystick joystick;
    float horizontalMove = 0f;
    public JoystickButton[] joyButton1;

    //STAT
    public static float health;
    public static float mana;
    public static float speed;
    public float startTimeBtwAttack;
    public int attack;

    //ATTACK
    Collider2D[] enemiesToDamage;
    public bool facingRight = true;
    public LayerMask whatIsEnemies;
    public Transform attackPos;
    public float attackRange;
    bool skillA;
    bool skillB;
    bool skillC;
    
    public PanZoom cameraShake;

    [SerializeField]
    private float timeBtwAttack;

    void Start()
    {
        health = 100;
        mana = 50;
        speed = 3.5f;
        GetComponent<thingsScript>();
        flipped = false;
    }

    void Update()
    {
        if (health < 0)
        {
            EnemyDestroyed();
        }

        if (joystick.Horizontal >= .2f || joystick.Vertical >= .2f)
        {
            horizontalMove = speed;
        }

        else if (joystick.Horizontal <= -.2f || joystick.Vertical <= -.2f)
        {
            horizontalMove = -speed;
        }

        else
        {
            horizontalMove = 0f;
        }

        movement.x = joystick.Horizontal;
        movement.y = joystick.Vertical;

        if (movement.x < 0)
            moveTemp = -1;

        if (movement.x > 0)
            moveTemp = 1;

        
        animator.SetFloat("Horizontal", movement.x);
        animator.SetFloat("speed", Mathf.Abs(horizontalMove));

        if(mana < 50)
        {
            mana += Time.deltaTime;
        }

        if (timeBtwAttack <= 0)
        {
            if (!skillB && joyButton1[1].Pressed)
            {
                skillB = true;
                StartCoroutine(cameraShake.Shake(.15f, .03f, this.transform));
                StartCoroutine(attackScene1());
                timeBtwAttack = startTimeBtwAttack;
                mana--;
            }

            if (skillB && !joyButton1[1].Pressed)
            {
                skillB = false;
            }

            if (!skillA && joyButton1[0].Pressed)
            {
                skillA = true;
                StartCoroutine(cameraShake.Shake(.15f, .03f, this.transform));
                StartCoroutine(attackScene2());
                timeBtwAttack = startTimeBtwAttack;
                mana--;
            }

            if(skillA && !joyButton1[0].Pressed)
            {
                skillA = false;
            }

            if (!skillC && joyButton1[2].Pressed)
            {
                skillC = true;
                StartCoroutine(attackScene3());
                StartCoroutine(cameraShake.Shake(.15f, .03f, this.transform));
                timeBtwAttack = startTimeBtwAttack;
                mana--;
            }

            if (skillC && !joyButton1[2].Pressed)
            {
                skillC = false;
            }
        }

        else if(timeBtwAttack > 0)
        {
            timeBtwAttack -= Time.deltaTime;
        }

        
    }

    IEnumerator attackScene1()
    {
        Debug.Log("start");
        animator.SetBool("isAttack", true);
        if (moveTemp == -1)
        {
            Flip();
            flipped = true;
        }
        else
        {
            flipped = false;
        }
        enemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange, whatIsEnemies);
        
        for (int i = 0; i < enemiesToDamage.Length; i++)
        {
            enemiesToDamage[i].GetComponent<Enemy1Script>().TakeDamage(attack);
        }
        yield return new WaitForSeconds(0.4f);
        animator.SetBool("isAttack", false);
        if(flipped)
        {
            Flip();
        }
    }

    IEnumerator attackScene2()
    {
        animator.SetBool("isAttack2", true);
        if (moveTemp == -1)
        {
            Flip();
            flipped = true;
        }
        else
        {
            flipped = false;
        }
        enemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange, whatIsEnemies);

        for (int i = 0; i < enemiesToDamage.Length; i++)
        {
            enemiesToDamage[i].GetComponent<Enemy1Script>().TakeDamage(attack);
        }
        yield return new WaitForSeconds(0.4f);
        animator.SetBool("isAttack2", false);
        if (flipped)
        {
            Flip();
        }
    }

    IEnumerator attackScene3()
    {
        Debug.Log("start");
        animator.SetBool("isAttack3", true);
        if (moveTemp == -1)
        {
            Flip();
            flipped = true;
        }

        else
        {
            flipped = false;
        }
        enemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange, whatIsEnemies);

        for (int i = 0; i < enemiesToDamage.Length; i++)
        {
            enemiesToDamage[i].GetComponent<Enemy1Script>().TakeDamage(attack);
        }
        StartCoroutine(cameraShake.Shake(.15f, .1f, this.transform));
        yield return new WaitForSeconds(0.8f);
        animator.SetBool("isAttack3", false);
        if (flipped)
        {
            Flip();
        }

    }

    public void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + movement * speed * Time.fixedDeltaTime);
    }

    public void EnemyDestroyed()
    {
        Destroy(this.gameObject);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPos.position, attackRange);
    }

    public void TakeDamage(float damages)
    {
        Debug.Log(health);
        Debug.Log("Player Damaged");
        health -= damages;
    }
}
