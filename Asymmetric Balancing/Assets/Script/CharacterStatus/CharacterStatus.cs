﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterStatus : MonoBehaviour
{
    // Start is called before the first frame update
    public static int level;
    public static int stat;
    public static int attack;
    public static int health;
    public static int attackSpeed;
    public static int speed;

    public Text levelText;
    public Text statText;
    public Text attackText;
    public Text healthText;
    public Text asText;
    public Text speedText;

    public GameObject player;
    void Start()
    {
        this.gameObject.SetActive(false);
        level = 5;
        stat = 25;
        GetComponent<DragToMove>();
    }

    // Update is called once per frame
    void Update()
    {
        levelText.GetComponent<Text>().text = level.ToString();
        statText.GetComponent<Text>().text = stat.ToString();
        attackText.GetComponent<Text>().text = attack.ToString();
        healthText.GetComponent<Text>().text = health.ToString();
        asText.GetComponent<Text>().text = attackSpeed.ToString();
        speedText.GetComponent<Text>().text = speed.ToString();
    }   
    
    public void showStats()
    {
        this.gameObject.SetActive(true);
    }

    public void closeStats()
    {
        this.gameObject.SetActive(false);
    }

    public void increaseAttack()
    {
        if(stat != 0)
        {
            stat -= 1;
            attack += 3;
            player.GetComponent<DragToMove>().attack += 1;
        }
        else
        {
            Debug.Log("You don't have enough stat");
        }
    }

    public void increaseHealth()
    {
        if (stat != 0)
        {
            stat -= 1;
            health += 2;
            DragToMove.health += 5f;
        }
        else
        {
            Debug.Log("You don't have enough stat");
        }
    }

    public void increaseAttackSpeed()
    {
        if (stat != 0)
        {
            stat -= 1;
            attackSpeed += 1;
            player.GetComponent<DragToMove>().startTimeBtwAttack -= 0.1f;
        }
        else
        {
            Debug.Log("You don't have enough stat");
        }
    }

    public void increaseSpeed()
    {
        if (stat != 0)
        {
            stat -= 1;
            speed += 1;
            DragToMove.speed += 0.2f;
        }
        else
        {
            Debug.Log("You don't have enough stat");
        }
    }
}
