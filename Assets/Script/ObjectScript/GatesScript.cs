﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GatesScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<WinScript>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            if(thingsScript.benda >= 10 && thingsScript.times <= 120)
            {
                WinScript.win3Star = true;
                Debug.Log("Player Win");
                SceneManager.LoadScene("WinScene");
            }

            if (thingsScript.benda >= 10)
            {
                WinScript.win3Star = true;
                Debug.Log("Player Win");
                SceneManager.LoadScene("WinScene");
            }

            if (thingsScript.benda >= 8 && thingsScript.benda <= 9)
            {
                WinScript.win2Star = true;
                Debug.Log("Player Win");
                SceneManager.LoadScene("WinScene");
            }

            if (thingsScript.benda >= 6 && thingsScript.benda <= 7)
            {
                WinScript.win1Star = true;
                Debug.Log("Player Win");
                SceneManager.LoadScene("WinScene");
            }

            else
            {
                Debug.Log("find more object");
            }

        }        
    }
}
