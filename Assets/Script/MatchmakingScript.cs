﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchmakingScript : MonoBehaviour
{
    public GameObject[] enemies;
    public GameObject Boss;
    public GameObject player;

    void Start()
    {
        GetComponent<DragToMove>();        
    }

    void Update()
    {
        StartCoroutine(Matchmaking());
    }  
    
    IEnumerator Matchmaking()
    {
        int pAttack = Random.Range(13, 19);
        player.GetComponent<DragToMove>().attack = pAttack;

        var pAttackSpeed = player.GetComponent<DragToMove>().startTimeBtwAttack;

        for (int i = 0; i < enemies.Length; i++)
        {
            if (pAttack <= 20)
            {
                enemies[i].GetComponent<Enemy1Script>().attack = Random.Range(7, 10);
                Boss.GetComponent<EnemyBossScript>().attack = Random.Range(16, 20);
            }

            else if (pAttackSpeed <= 1)
            {
                enemies[i].GetComponent<Enemy1Script>().startAttack = 1;
            }

            if (DragToMove.health <= 100)
            {
                enemies[i].GetComponent<Enemy1Script>().health = Random.Range(40, 43);
                Boss.GetComponent<EnemyBossScript>().health = Random.Range(490, 500);
            }

            if (DragToMove.health <= 115)
            {
                enemies[i].GetComponent<Enemy1Script>().health = Random.Range(40, 43);
                Boss.GetComponent<EnemyBossScript>().health = Random.Range(500, 515);
            }

            if (DragToMove.health <= 130)
            {
                enemies[i].GetComponent<Enemy1Script>().health = Random.Range(40, 43);
                Boss.GetComponent<EnemyBossScript>().health = Random.Range(515, 520);
            }

            if (DragToMove.speed <= 2.5f)
            {
                enemies[i].GetComponent<Enemy1Script>().speed = Random.Range(2.1f, 2.5f);
                Boss.GetComponent<EnemyBossScript>().speed = Random.Range(1.1f, 1.4f);
            }

            if (DragToMove.speed <= 3f)
            {
                enemies[i].GetComponent<Enemy1Script>().speed = Random.Range(2.6f, 2.9f);
                Boss.GetComponent<EnemyBossScript>().speed = Random.Range(1.6f, 1.9f);
            }

            if (DragToMove.speed <= 3.5f)
            {
                enemies[i].GetComponent<Enemy1Script>().speed = Random.Range(3.1f, 3.5f);
                Boss.GetComponent<EnemyBossScript>().speed = Random.Range(2.1f, 2.4f);
            }

        }
        yield return new WaitForSeconds(2);
        Destroy(this.gameObject);
    }
}
