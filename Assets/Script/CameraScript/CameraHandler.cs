﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraHandler : MonoBehaviour
{
    // Start is called before the first frame update
    public PanZoom cameraFollow;
    public Transform playerTransform;
    private void Start()
    {
        cameraFollow.Setup(() => playerTransform.position);
    }
}
