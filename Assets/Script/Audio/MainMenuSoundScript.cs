﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuSoundScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        FindObjectOfType<AudioManagerScript>().Play("BGM");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
