﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ModalOpen : MonoBehaviour
{
    // Start is called before the first frame update
    public static bool modalOpen;
    public GameObject pauseModal;
    void Start()
    {
        modalOpen = false;
        pauseModal.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pauseButton()
    {
        if(!modalOpen)
        {
            pauseModal.SetActive(true);
            modalOpen = true;
        }
    }

    public void cancelModal()
    {
        if(modalOpen)
        {
            pauseModal.SetActive(false);
            modalOpen = false;
        }
    }

    public void BacktoMenu()
    {
        SceneManager.LoadScene("MainMenuScene");
    }
}
