﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MoveSceneScript : MonoBehaviour
{
    public string sceneLoaded;

    public void TargetMoveScene()
    {
        SceneManager.LoadScene(sceneLoaded);
    }

    public void PlayScene()
    {
        FindObjectOfType<AudioManagerScript>().Stop("BGM");
    }

    public void BackScene()
    {
        SceneManager.LoadScene("MainMenuScene");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
